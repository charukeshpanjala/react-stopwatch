import React from 'react';
import clock from './clock.svg';
import './App.css';
import Stopwatch from "./components/index.js";
function App() {
  return (
    <div className="App">
      <header className="App-header">
        STOP<span class="App-span">WATCH</span>
        <img src={clock} className="App-logo" alt="logo" />
      </header>
      <Stopwatch />
    </div>
  );
}

export default App;
