import React, { Component } from "react";
import "./index.css";

class Stopwatch extends Component {
    constructor() {
        super()
        this.state = {
            timeInMilliSeconds: 0,
            pause: false
        }
    }

    updateTime = () => {
        return setInterval(() => {
            this.setState(prevState => ({ timeInMilliSeconds: prevState.timeInMilliSeconds + 1 }))
        }, 10)
    }

    onClickStart = () => {
        if (this.state.timeInMilliSeconds === 0) {
            clearInterval(this.interval)
            this.interval = this.updateTime()
        }
    }

    pausePlay = () => {
        clearInterval(this.interval)
        if (!this.state.pause) {
            this.interval = this.updateTime()
        }
    }

    onClickPause = () => {
        this.setState(prevState => ({ pause: !prevState.pause }), this.pausePlay)
    }

    onClickReset = () => {
        clearInterval(this.interval)
        this.setState({ timeInMilliSeconds: 0, pause: false })
    }

    renderTimer = (time) => {
        let minutes = Math.floor((time / (100 * 60)))
        const seconds = Math.floor((time / 100))
        const convertedSeconds = seconds < 60 ? seconds : seconds - (60 * minutes)
        const milliseconds = time < 100 ? time : time - 100 * (seconds)
        const stringMins = minutes > 9 ? minutes : `0${minutes}`
        const stringSecs = convertedSeconds > 9 ? convertedSeconds : `0${convertedSeconds}`
        const stringMilliSecs = milliseconds > 9 ? milliseconds : `0${milliseconds}`
        return (
            <div className="timer-container">
                <p className="time">{stringMins}</p>
                <p className="time" style={{ border: 0 }}>:</p>
                <p className="time">{stringSecs}</p>
                <p className="time" style={{ border: 0 }}>:</p>
                <p className="time">{stringMilliSecs}</p>
            </div>
        )
    }

    render() {
        return (
            <div className="stopwatch-container">
                {this.renderTimer(this.state.timeInMilliSeconds)}
                <button className="button" onClick={this.onClickStart}>Start</button>
                <button className="button" onClick={this.onClickPause}>Pause/Play</button>
                <button className="button" onClick={this.onClickReset}>Reset</button>
            </div>
        )
    }
}

export default Stopwatch